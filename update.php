<?php
    define('_NOIZYEXEC', true); 
    require_once('boot.php');
    require_once('functions.php');

    // Configuration unreadable
    if (!import_ini_file(CONFIG_FILE)) { 
        echo "Can't read config file, process to install please";
    }
    else {
        check_n_publish(RSS_SOURCE);
    }
?> 
