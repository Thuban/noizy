<?php

function shorten_huitre($link) {
    $lien = 'https://huit.re/a';

    $postfields = array(
        'lsturl' => $link,
        'format' => 'json'
    );


    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $lien);
    curl_setopt($curl, CURLOPT_COOKIESESSION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);

    $return = curl_exec($curl);
    curl_close($curl);

    $results = array();
    $results = json_decode($return, true);

    if ($results['success'] == 'true') {
        return $results['short'];
    }
    else {
        return false;
    }

}
?>
