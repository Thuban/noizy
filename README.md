NOIZY
=====

This is a small PHP page to automatically relay new RSS feeds to various social websites, such as mastodon.
You'll need php-curl to use it.

It is first intended to use with a
[shaarli](https://github.com/shaarli/Shaarli) or
[blogotext](https://blogotext.org/) instance.

Usage
-----

Clone or unzip files, then upload (I.E. with a ftp client) to any directory of your server. Go to ``https://yoursite.com/whatever/noizy/index.php``) to configure.

Once configured, go again to this page to publish new RSS item. you can use a cron job like this : 

	0 * * * * /usr/local/bin/curl "https://yoursite.com/whatever/noizy/update.php" > /dev/null 2>&1


Warning
-------
Disable acces to everything except index.php and install.php.
In httpd (openbsd) : 


        location "/whatever/noizy/data*" { block }
        location "/whatever/noizy/engines*" { block }


Used in this project 
----------------------

- icon : https://openclipart.org/detail/284500/hey-you
- Mastodon library : TootoPHP https://framagit.org/MaxKoder/TootoPHP
- Shaarlitotwitter :
  https://github.com/ArthurHoaro/shaarli2twitter/tree/master/shaarli2twitter


Contributions
------------

Do not hesitate to add more engines.

TODO
-----
- Add diaspora* engine
- Add twitter engine -> done, to check
- Add archive.org engine
- Short link option with huit.re -> done
