function getInstance(instance_id, engine) {
	instance = document.getElementById(instance_id).value;
	instance = instance.replace('https://', '');
	instance = instance.replace('http://', '');
	window.open('install.php?registerApp='+engine+'&instance='+instance, "_blank");
};

function enable_token_btn(val, btn_id) {
	btn = document.getElementById(btn_id);
	if (val != "" ) {btn.disabled = false;}
	else {btn.disabled = true;}
};

function toggle_visibility(id) {
// https://css-tricks.com/snippets/javascript/showhide-element/
	var e = document.getElementById(id);
	if(e.style.display == 'block')
	{
		e.style.display = 'none';
		e.style.opacity = '0';
	}
	else
	{
		e.style.display = 'block';
		e.style.opacity = '1';
	}
}
